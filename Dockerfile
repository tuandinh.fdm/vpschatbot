# set base image (host OS)
FROM python:3.7

# set the working directory in the container
WORKDIR /code

COPY requirement.txt .

RUN pip install -r requirement.txt --extra-index-url https://pypi.python.org/simple --no-dependencies

COPY . .

EXPOSE 5005
EXPOSE 5055
# command to run on container start

