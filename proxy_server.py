import requests
import os
from flask import Flask, request

SERVER_URL = os.getenv('SERVER_URL', 'http://localhost:3000')
app = Flask(__name__)


@app.route('/api/v1/login', methods=['POST'])
def index():
    username = request.form.get('username')

    password = request.form.get('password')

    # if username == 'tuan':
    #
    #     return {'status': 'ok'}
    #
    # else:
    #
    #     return 'error', 401
    response = requests.post(SERVER_URL + '/api/v1/login', data={'username':username, 'password':password}, verify=False)

    if response.ok:
        return response.json()
    else:
        return response.reason, response.status_code

app.run(port=8800, debug=True)
