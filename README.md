Project move to https://gitlab.com/fdmcorp/rasa-chatbots/vpschatbot.git

**Chatbot FDM**

**How to run**

Create virtual enviroment
~~~~
conda create --name myenv
~~~~
Active virtual enviroment
~~~~
conda activate myenv
~~~~
Install package
~~~~
conda [path_to_conda]/envs/myenv/bin/pip3 install -r requirement.txt

install spacy cho tiếng việt
pip install https://github.com/trungtv/vi_spacy/raw/master/packages/vi_spacy_model-0.2.1/dist/vi_spacy_model-0.2.1.tar.gz
pip install spacy==2.1.0
python3 -m spacy link vi_spacy_model vi_spacy_model


~~~~
Run Rasa Server
~~~~
python __main__.py run actions -vv
~~~~
Run Rasa 
~~~~
python __main__.py run -m models/ --enable-api
Nếu chạy với model word2vec/spacy
python __main__.py run -m models/ --enable-api --config vi_spacy_config.yml


~~~~
API parameters

`http://localhost:5005/webhooks/rest/webhook`

POST params:

`{"message":"xin chào", "sender":"tuandh"}`

Run test

`curl --location --request POST 'http://localhost:5005/webhooks/rest/webhook'
--header 'Content-Type: text/plain'
--data-raw '{"message":"xin chào", "sender":"tuandh"}'`


