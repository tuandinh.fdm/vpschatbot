##faq_0
* faq/faq_0
  -  Để mở TKCK tại VPS, quý khách vui lòng thực hiện một trong hai cách sau: \nCách 1: Quý khách truy cập vào trang: https://openaccount.vps.com.vn/Home/OpenAccount, VPS sẽ tiếp nhận thông tin và hỗ trợ khách hàng hoàn tất thủ tục mở tài khoản.\nCách 2: Quý khách mang theo CMT/CCCD còn thời hạn đến trực tiếp Trụ sở/Chi nhánh của VPS để mở tài khoản.

##faq_1
* faq/faq_1
  -  Quý khách vui lòng tham khảo Hướng dẫn nộp tiền theo link dưới đây:\nhttps://www.vps.com.vn/Sites/QuoteVN/SiteRoot/documents/VPS_HuongDanNopTienTKCK_20190110.pdf

##faq_2
* faq/faq_2
  -  Quý khách vui lòng mang CMND/CCCD lên trực tiếp trụ sở/chi nhánh của VPS để thực hiện yêu cầu đóng  tài khoản.\n(Đối với tài khoản có đăng ký các sản phẩm ký quỹ, khách hàng cần hoàn trả các nghĩa vụ nợ. Khi tài sản trên TKCK bằng 0, khách hàng mới thực hiện đóng được TK).

##faq_3
* faq/faq_3
  -  Quý khách vui lòng mang CMTND/CCCD, Sổ cổ đông hoặc Giấy chứng nhận sở hữu cổ phần tới trực tiếp Trụ sở/Chi nhánh của VPS để được hỗ trợ mở tài khoản chứng khoán và thực hiện lưu ký chứng khoán.

##faq_4
* faq/faq_4
  -  Để mở tài khoản chứng khoán cho nhà đầu tư nước ngoài, quý khách vui lòng cung cấp:\n- Bản sao công chứng hộ chiếu còn hiệu lực;\n- Bản chụp mẫu mở tài khoản vốn đầu tư gián tiếp nếu quý khách không cư trú tại VN. 

##faq_5
* faq/faq_5
  -  Hiện tại theo quy định của Trung tâm Lưu ký Chứng khoán, khách hàng là công dân Việt Nam bắt buộc sử dụng CMTND/ CCCD để mở tài khoản giao dịch và không sử dụng Passport để mở tài khoản

##faq_6
* faq/faq_6
  -  Theo quy định của Ủy ban Chứng khoán Nhà nước, một khách hàng chỉ có thể sử dụng một CMND/CCCD để mở một tài khoản tại một công ty chứng khoán. 

##faq_7
* faq/faq_7
  -  Quý khách vui lòng mang theo CMND mới, CMND cũ (hộ chiếu có số CMND cũ/ xác nhận của cơ quan công an CMND và thẻ CCCD là của 1 người) tới trực tiếp Trụ sở/Chi nhánh của VPS để thực hiện các thủ tục thay đổi thông tin.

##faq_8
* faq/faq_8
  -  Tại VPS, một tài khoản chứng khoán sẽ được chia làm các tiểu khoản nhỏ:\n- Tiểu khoản 1: Dành riêng cho giao dịch chứng khoán cơ sở thông thường và các sản phẩm quản lý tài sản khác.\n- Tiểu khoản 3,6: Dành riêng cho giao dịch ký quỹ\n- Tiểu khoản 8: Dành cho giao dịch chứng khoán phái sinh\nQuý khách có thể thực hiện chuyển đổi giữa các tiểu khoản và giao dịch. 

##faq_9
* faq/faq_9
  -  Quý khách và người ủy quyền vui lòng mang theo CMT/CCCD còn thời hạn tới trực tiếp Trụ sở/chi nhánh của VPS để hoàn thành thủ tục ủy quyền.

##faq_10
* faq/faq_10
  -  Quý khách có thể tham khảo thông tin sản phẩm, biểu phí, cũng như hướng dẫn giao dịch Chứng khoán phái sinh theo đường dẫn dưới đây:\n- Thông tin sản phẩm: https://www.vps.com.vn/DerivativeBasic.aspx\n- Hướng dẫn giao dịch: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/Documents/Phaisinh/huong_dan_giao_dich_chung_khoan_phai_sinh.pdf\n- Biểu phí: https://www.vps.com.vn/bieu-phi-giao-dich-ck-phai-sinh.aspx

##faq_11
* faq/faq_11
  -  Để mở TKCK Phái sinh tại VPS, quý khách vui lòng thực hiện theo hướng dẫn sau: \n1. Đối với khách hàng chưa có Tài khoản chứng khoán cơ sở: \n- Quý khách truy cập vào trang chủ VPS mục Mở tài khoản trực tuyến (https://openaccount.vps.com.vn/Home/OpenAccount) và điền đầy đủ thông tin cá nhân, VPS sẽ tiếp nhận và gửi lại cho khách hàng hợp đồng mở tài khoản vào email khách hàng đăng ký.\nTrong hợp đồng Mở tài khoản chứng khoán (MTK), khách hàng ký vào mục MTK Chứng khoán phái sinh.\n- Hoặc Quý khách mang theo CMT/CCCD đến trực tiếp Trụ sở/Chi nhánh của VPS để mở tài khoản.\n2. Đối với Khách hàng đã có TKCK cơ sở:\nQuý khách vui lòng truy cập các đường dẫn sau để đăng ký MTK chứng khoán Phái sinh online: https://smartpro.vps.com.vn/, https://smartone.vps.com.vn/Account/Login hoặc tải App SmartOne, SmartPro.\nHoặc Quý khách mang theo CMT/CCCD đến trực tiếp Trụ sở/Chi nhánh của VPS để mở tài khoản.

##faq_12
* faq/faq_12
  -  Quý khách làm theo hướng dẫn sau để giao dịch chứng khoán phái sinh: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/documents/12%20NDTCanLamGiKhiGDCKPS.pdf

##faq_13
* faq/faq_13
  -  Để giao dịch Chứng khoán phái sinh một cách thuận lợi, quý khách cần lưu ý một số điều cơ bản như sau:\nhttps://www.vps.com.vn/Sites/QuoteVN/SiteRoot/documents/13%20NDTCanLuuYGiKhiGDCKPS.pdf

##faq_14
* faq/faq_14
  -  Để giao dịch chứng khoán phái sinh tại VPS, quý khách vui lòng truy cập website: \nhttps://smartpro.vps.com.vn/Account/Login\nHướng dẫn giao dịch xem tại: https://smartpro.vps.com.vn/v1/doc/HDSD_SmartPro.pdf\n\nHoặc Quý khách có thể giao dịch trên điện thoại bằng cách tải ứng dụng SmartPro.\n\nNgoài ra, Quý khách có thực hiện đặt lệnh giao dịch chứng khoán phái sinh qua tổng đài 19006457 phím 1 của VPS

##faq_15
* faq/faq_15
  -  Để tìm hiểu cách tính lãi/lỗ trên một Hợp đồng tương lai, quý khách vui lòng truy cập link: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/documents/11%20LaiLoKhiGDCKPSDuocTinhToanntn.pdf

##faq_16
* faq/faq_16
  -  Tới thời điểm cuối ngày, toàn bộ lãi/lỗ đối với các vị thế đã đóng và lãi/lỗ đối với các vị thế mở đều được ghi nhận và thanh toán cho khách hàng.\nCuối mỗi ngày giao dịch, một mức giá thanh toán cuối ngày được xác định làm cơ sở để Trung tâm Bù trừ hạch toán lãi/lỗ cho các giao dịch phát sinh trong ngày hoặc giá trị của các vị thế đang nắm giữ như sau:\n- Với những hợp đồng mở mới trong ngày:\nLãi/lỗ vị thế mới mở = (Giá thanh toán cuối ngày – Giá giao dịch) x Hệ số nhân x Số lượng hợp đồng\n- Với những hợp đồng đang nắm giữ:\nLãi/lỗ với với vị thế đã có = (Giá thanh toán cuối ngày – Giá thanh toán cuối ngày liền trước) x Hệ số nhân x Số lượng hợp đồng.

##faq_17
* faq/faq_17
  -  CKPS được thanh toán bù trừ hàng ngày. Cụ thể:\n- Nếu tài khoản chứng khoán phái sinh của NĐT lỗ ròng: NĐT sẽ phải thanh toán đầy đủ toàn bộ số lỗ phát sinh chậm nhất đến 8h30 sáng ngày hôm sau.\n- Nếu tài khoản chứng khoán phái sinh của NĐT lãi ròng: NĐT sẽ nhận được đầy đủ số tiền lãi phát sinh sau 9h sáng ngày hôm sau.

##faq_18
* faq/faq_18
  -  Vào ngày đáo hạn của HĐTL nếu Quý khách không đóng vị thế, hợp đồng sẽ tự thanh toán theo giá đóng cửa của chỉ số cơ sở VN30.

##faq_19
* faq/faq_19
  -  Hiện tại, tỷ lệ ký quỹ tại VPS là 16.4%.

##faq_20
* faq/faq_20
  -  Hiện tại, thông số quản lý tài khoản giao dịch chứng khoán phái sinh như sau:\n- Ngưỡng cảnh báo 1 (Wa1): 90% - Tài khoản bị yêu cầu bổ sung tài sản ký quỹ để đưa tỷ lệ sử dụng tài sản về mức < 80%\n- Ngưỡng cảnh báo 2 (Wa2): 95% -  Tài khoản bị bắt buộc phải xử lý để đưa tỷ lệ sử dụng tài sản về mức < 80%

##faq_21
* faq/faq_21
  -  Quý khách vui lòng tham khảo thông tin các dịch vụ và biểu phí áp dụng cho Tài khoản chứng khoán cơ sở tại các địa chỉ dưới đây:\n- Hướng dẫn cơ bản: https://www.vps.com.vn/dich-vu-chung-khoan-ho-tro-giao-dich.aspx\n- Dịch vụ: https://www.vps.com.vn/dich-vu-chung-khoan-gioi-thieu-dich-vu.aspx\n- Biểu phí: https://www.vps.com.vn/dich-vu-chung-khoan-bieu-phi-dich-vu.aspx

##faq_22
* faq/faq_22
  -  Quý khách vui lòng tham khảo khung giờ giao dịch do Ủy ban Chứng khoán Nhà nước quy định tại đây: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/Documents/HuongDan/Huong_dan_co_ban_ve_giao_dich_chung_khoan.docx

##faq_23
* faq/faq_23
  -  Hiện tại, chu kỳ thanh toán tiền cho cổ phiếu đang áp dụng trên cả 3 sàn HOSE, HNX và UPCOM đang là T+2 (ngày làm việc). \nTuy nhiên, Nhà đầu tư có thể thực hiện ứng trước tiền bán ngay trong ngày thông qua dịch vụ Ứng trước tiền bán của VPS. Mức phí cho dịch vụ Ứng trước tiền bán là 14%/năm và miễn lãi ngày T0. 

##faq_24
* faq/faq_24
  -  Để giao dịch cổ phiếu tại VPS, quý khách có thể truy cập website: https://smartone.vps.com.vn/Account/Login.\nChi tiết xem tại: https://smartone.vps.com.vn/Templates/Huong_dan_su_dung_giao_dich_truc_tuyen.pdf\nHoặc giao dịch trên điện thoại thông minh bằng cách tải ứng dụng SmartOne.

##faq_25
* faq/faq_25
  -  Dịch vụ Giao dịch ký quỹ là tiện ích cho phép khách hàng có thể dùng một phần tiền vay từ VPS trên cơ sở ký quỹ bằng chính chứng khoán mua hoặc chứng khoán hiện có trên tài khoản giao dịch:\n- Tỷ lệ giải ngân tối đa 50%\n- Miễn lãi ngày T0\n- Lãi suất vay 14%/năm, lãi suất quá hạn 150% lãi suất trong hạn\n\nĐể tìm hiểu thêm về dịch vụ giao dịch ký quỹ tại VPS, quý khách truy cập: https://www.vps.com.vn/FinancialServices.aspx

##faq_26
* faq/faq_26
  -  Để đăng ký sản phẩm giao dịch ký quỹ, quý khách vui lòng qua trực tiếp trụ sở/chi nhánh của VPS để mở tài khoản ký quỹ hoặc liên hệ với nhân viên Quản lý tài khoản để được hỗ trợ.

##faq_27
* faq/faq_27
  -  Gói V5%, V9% và V70+ là sản phẩm ký quỹ chuyên biệt sử dụng trên tiểu khoản 3 tại VPS. Quý khách vui lòng đến trực tiếp quầy giao dịch của VPS hoặc liên hệ nhân viên QLTK để mở tiểu khoản 3 và sử dụng các sản phẩm ký quỹ chuyên biệt này.

##faq_28
* faq/faq_28
  -  Quý khách tham khảo danh mục Chứng khoán giao dịch ký quỹ theo đường link sau: https://www.vps.com.vn/danh_muc_chung_khoan_gdkq.aspx

##faq_29
* faq/faq_29
  -  Quý khách vui lòng tham khảo thông tin về Chứng quyền có bảo đảm tại link dưới đây:\nhttps://www.vps.com.vn/CoveredBasic.aspx

##faq_30
* faq/faq_30
  -  Qúy khách vui lòng mở tài khoản chứng khoán để thực hiện giao dịch Chứng quyền tại VPS. Chứng quyền được giao dịch trên tài khoản chứng khoán cơ sở (tương tự như giao dịch cổ phiếu).

##faq_31
* faq/faq_31
  -  Sản phẩm MM giúp cho nhà đầu tư tối ưu nguồn tiền trong ngắn hạn thay vì để nhàn rỗi trên tài khoản và hưởng lãi suất không kỳ hạn. \nKhách hàng có thể tham gia sản phẩm MM với số tiền chỉ từ 1 triệu đồng, kỳ hạn từ 2 ngày đến 365 ngày và nhận tỷ suất lợi tức cam kết rất hấp dẫn.\n\nQuý khách tham gia sản phẩm MM online tại: https://smartone.vps.com.vn/#Mm-Introduction/Introduction hoặc tải App SmartOne --> vào mục SP tài chính --> MM\nQuý khách cũng có thể tới trực tiếp trụ sở VPS để giao dịch. 

##faq_32
* faq/faq_32
  -  Quý khách đang quan tâm đến kỳ hạn bao nhiêu ngày ạ?\nTự typing trả lời mức LS\n\nĐể tham khảo tỷ suất lợi tức của các kỳ hạn khác, Quý khách vui lòng truy cập: https://smartone.vps.com.vn/#Mm-Contracting/Contracting\nHoặc tải App SmartOne --> SP tài chính --> MM --> Lập giao dịch

##faq_33
* faq/faq_33
  -  Để giao dịch sản phẩm MM online, Quý khách vui lòng truy cập https://smartone.vps.com.vn/#Mm-Contracting/Contracting Hoặc tải App SmartOne --> SP tài chính --> MM --> Lập giao dịch.\n- Bước 1: Quý khách nộp tiền vào TKCK, tham khảo Hướng dẫn nộp tiền tại đây: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/Documents/Huong_dan_nop_tien_tai_khoan_chung_khoan.pdf\n- Bước 2: Ký online “Hợp đồng tối ưu hóa nguồn vốn” (Đối với khách hàng tham gia lần đầu).\n- Bước 3: Vào mục "Lập giao dịch" nhập số tiền đầu tư (tối thiểu 1 triệu) và chọn Ngày đáo hạn (tối thiểu từ 2 ngày).\n- Bước 4: Nhập mã Pin và OTP để xác nhận tham gia sản phẩm. VPS gửi xác nhận giao dịch thành công qua email của khách hàng. \n\nQuý khách cũng có thể tới trực tiếp trụ sở VPS để giao dịch. 

##faq_34
* faq/faq_34
  -  Quý khách có thể giao dịch sản phẩm MM từ 8h30 đến 17h các ngày làm việc.

##faq_35
* faq/faq_35
  -  - Giao dịch online: hạn mức tối đa là 20 tỷ. \n- Giao dịch trực tiếp tại VPS: không quy định hạn mức tối đa. 

##faq_36
* faq/faq_36
  -  Quý khách có thể Rút ngắn thời hạn một phần hoặc toàn bộ khoản đầu tư và nhận mức lãi suất không kỳ hạn (0,5%/năm) đối với phần tiền rút trước.  \n\nQuý khách vui lòng thao tác Rút ngắn thời hạn online tại: https://smartone.vps.com.vn/#Mm-ContractList/ContractList hoặc Đăng nhập App SmartOne --> SP tài chính --> MM --> Danh mục --> Điều chỉnh giao dịch.\nHoặc Quý khách vui lòng tới trực tiếp trụ sở VPS để thực hiện. 

##faq_37
* faq/faq_37
  -  - Lợi tức của sản phẩm MM thông báo đến khách hàng là lợi tức thực nhận, khách hàng không bị trừ thuế TNCN.\n- Khi chuyển tiền cùng hệ thống 4 ngân hàng liên kết: VPBank, BIDV, Sacombank, Vietinbank, khách hàng được miễn phí chuyển tiền.

##faq_38
* faq/faq_38
  -  Phương án đầu tư/sử dụng nguồn tiền của sản phẩm MM có thể là đầu tư vào công cụ thị trường tiền tệ với mức độ an toàn cao như giấy tờ có giá (trái phiếu Chính phủ, trái phiếu được Chính phủ bảo lãnh hay chứng chỉ tiền gửi của tổ chức tín dụng) trong ngắn hạn hoặc phương án khác. \nKhi hết thời hạn đầu tư sản phẩm MM, khách hàng sẽ nhận đủ vốn gốc và lợi tức đã cam kết. 

##faq_39
* faq/faq_39
  -  Trái phiếu doanh nghiệp là sản phẩm cho thu nhập cố định và có thể thay thế cho sản phẩm đầu tư truyền thống. Các sản phẩm Trái phiếu được VPS lựa chọn kỹ càng dựa trên nhu cầu đầu tư của khách hàng.\n\nQuý khách đầu tư Trái phiếu online tại: https://smartone.vps.com.vn/#Bond-Introduction/Introduction hoặc tải App SmartOne --> vào mục SP tài chính --> Trái phiếu\nQuý khách cũng có thể tới trực tiếp trụ sở VPS để giao dịch. 

##faq_40
* faq/faq_40
  -  Hiện tại VPS tập trung cho 3 sản phẩm Trái phiếu chính:\n- Trái phiếu cố định: Nhà đầu tư nắm giữ trái phiếu với thời hạn cố định và sẽ được VPS hỗ trợ thanh khoản vào cuối thời hạn. \n- Trái phiếu linh hoạt: Nhà đầu tư sẽ nắm giữ trái phiếu trái phiếu với kỳ hạn nhất định nhưng sẽ được VPS hỗ trợ tất toán trước hạn khi có nhu cầu và vẫn được hưởng lãi suất theo biểu lũy thoái. \n- Trái phiếu Outright: Sản phẩm này hướng đến khách hàng có nhu cầu mua và nắm giữ trái phiếu đến khi đáo hạn để hưởng lợi tức ưu việt hơn. Tuy nhiên, trên thực tế, nếu có nhu cầu thoái vốn trước hạn thì nhà đầu tư có thể bán sớm trái phiếu qua hệ thống của VPS - khi đó, VPS sẽ nỗ lực hỗ trợ (dù không cam kết) về thanh khoản cho nhà đầu tư.

##faq_41
* faq/faq_41
  -  Hiện tại VPS đang có nhiều sản phẩm Trái phiếu với mức sinh lời và kỳ hạn khác nhau. Quý khách vui lòng tham khảo tại link: https://smartone.vps.com.vn/#Bond-Contracting/Contracting    Hoặc tải App SmartOne --> SP tài chính --> Trái phiếu --> Đầu tư mới.

##faq_42
* faq/faq_42
  -  Hiện tại VPS đang có nhiều sản phẩm Trái phiếu với mức sinh lời và kỳ hạn khác nhau. Quý khách vui lòng tham khảo tại link: https://smartone.vps.com.vn/#Bond-Contracting/Contracting    Hoặc tải App SmartOne --> SP tài chính --> Trái phiếu --> Đầu tư mới.

##faq_43
* faq/faq_43
  -  Các Trái phiếu tại VPS đều có tài sản đảm bảo hoặc có Ngân hàng bảo lãnh thanh toán. Khi Hợp đồng Trái phiếu đáo hạn, khách hàng nhận lại 100% vốn gốc và mức sinh lời đã cam kết. 

##faq_44
* faq/faq_44
  -  - Đối với khách hàng cá nhân: Mức sinh lời VPS thông báo là mức thực nhận, Quý khách không cần trả thêm bất kỳ khoản phí nào. \n- Đối với khách hàng doanh nghiệp: Khách hàng sẽ chi trả thuế Thu nhập doanh nghiêp.

##faq_45
* faq/faq_45
  -  Để giao dịch sản phẩm Trái phiếu online, Quý khách vui lòng truy cập: https://smartone.vps.com.vn/#Bond-Contracting/Contracting Hoặc tải App SmartOne --> SP tài chính --> Trái phiếu --> Đầu tư mới.\n- Bước 1: Quý khách nộp tiền vào TKCK, tham khảo Hướng dẫn nộp tiền tại đây: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/Documents/Huong_dan_nop_tien_tai_khoan_chung_khoan.pdf\n- Bước 2: Chọn sản phẩm Trái phiếu, thao tác ký “Hợp đồng khung mua, bán Trái phiếu” online với VPS (Đối với khách hàng tham gia lần đầu).\n- Bước 3: Vào mục "Đầu tư mới" để chọn các thông tin đầu tư và tham khảo mức sinh lời. \n- Bước 4: Nhập mã Pin và OTP để xác nhận tham gia sản phẩm. VPS gửi xác nhận giao dịch bằng văn bản tới khách hàng (nếu KH có nhu cầu).  \n\nHoặc Quý khách vui lòng tới trực tiếp trụ sở VPS để giao dịch. 

##faq_46
* faq/faq_46
  -  Quý khách đầu tư Trái phiếu linh hoạt tại VPS có thể tất toán Hợp đồng vào bất kỳ thời điểm nào và vẫn hưởng mức sinh lời theo biểu lũy thoái. \nQuý khách có thể thao tác online tại: https://smartone.vps.com.vn/#Bond-Contractlist/Contractlist hoặc tải App SmartOne --> SP tài chính --> Trái phiếu --> Danh mục --> Điều chỉnh giao dịch.\n\nNgoài ra, đối với các sản phẩm Trái phiếu khác, VPS luôn có chính sách hỗ trợ giúp cho khách hàng linh động về dòng tiền. Quý khách vui lòng liên hệ tổng đài 19006457 hoặc tới trực tiếp trụ sở VPS để được tư vấn chi tiết. 

##faq_47
* faq/faq_47
  -  Chương trình Gửi tiền tại các NHTM là chương trình mà VPS cung cấp để hỗ trợ cho Khách Hàng gửi tiền có kỳ hạn tại các Ngân hàng thương mại đang liên kết với VPS, cụ thể là 4 ngân hàng: BIDV, OCB, TPBank, VPBank.\n\nĐể tham gia Tiền gửi kỳ hạn online, Quý khách vui lòng truy cập: https://smartone.vps.com.vn/#Deposit-Request/Request hoặc tải App SmartOne --> vào mục SP tài chính --> Tiền gửi tại NHTM\nQuý khách cũng có thể tới trực tiếp trụ sở VPS để giao dịch. 

##faq_48
* faq/faq_48
  -  Khách hàng sẽ hưởng mức lãi suất tối ưu trong hệ thống của từng Ngân hàng thương mại hợp tác. \n\nĐể tham khảo mức lãi suất chi tiết, Quý khách vui lòng truy cập: https://smartone.vps.com.vn/#Deposit-Information/Information hoặc tải App SmartOne -->  vào mục SP tài chính --> Tiền gửi tại NHTM --> Lãi suất. 

##faq_49
* faq/faq_49
  -  Thời gian ngân hàng nhận yêu cầu giao dịch chậm nhất vào 15h30 các ngày làm việc. 

##faq_50
* faq/faq_50
  -  Để tham gia Tiền gửi kỳ hạn online, Quý khách vui lòng truy cập: https://smartone.vps.com.vn/#Deposit-Request/Request hoặc tải App SmartOne --> vào mục SP tài chính --> Tiền gửi tại NHTM --> Đối tác\n- Bước 1: Quý khách nộp tiền vào TKCK, tham khảo Hướng dẫn nộp tiền tại đây: https://www.vps.com.vn/Sites/QuoteVN/SiteRoot/Documents/Huong_dan_nop_tien_tai_khoan_chung_khoan.pdf\n- Bước 2: Chọn sản phẩm Tiền gửi kỳ hạn tại NHTM\n- Bước 3: Điền các thông tin gửi tiền và tham khảo mức lãi suất. \n- Bước 4: Nhập mã Pin và OTP để xác nhận tham gia sản phẩm.\n\nHoặc Quý khách vui lòng tới trực tiếp trụ sở VPS để giao dịch. 

##faq_51
* faq/faq_51
  -  Quý khách vui lòng tra cứu khoản tiền gửi tại: https://smartone.vps.com.vn/#Deposit-Trading/Trading hoặc Đăng nhập SmartOne --> vào mục SP tài chính --> Tiền gửi tại NHTM --> DS Tiền gửi.\n\nVPS có thể cung cấp chứng từ Thông báo số dư tiền gửi có kỳ hạn (nếu khách hàng yêu cầu). 

##faq_52
* faq/faq_52
  -  Quý khách có thể rút gốc trước hạn và nhận lãi suất không kỳ hạn (theo giờ giao dịch quy định của từng ngân hàng). \n\nQuý khách vui lòng thao tác online tại:  https://smartone.vps.com.vn/#Deposit-Trading/Trading hoặc đăng nhập App SmartOne --> Tiền gửi tại NHTM --> DS tiền gửi --> Điều chỉnh giao dịch.\nHoặc Quý khách vui lòng tới trực tiếp trụ sở VPS để thực hiện. 

##faq_53
* faq/faq_53
  -  Các sản phẩm Bảo hiểm sức khỏe tại VPS được thiết kế riêng biệt cho khách hàng của công ty với chi phí bảo hiểm cạnh tranh, quyền lợi đa dạng và thủ tục bồi thường nhanh chóng. Hiện tại chúng tôi đang liên kết với các đối tác:\n1. Bảo hiểm Bưu Điện\n2. Bảo hiểm VietinBank\n3. Bảo hiểm Bảo Việt \n\nĐể tham khảo thông tin chi tiết, Quý khách vui lòng tải App SmartOne --> vào mục SP tài chính --> Bảo hiểm.\nNgoài ra, Quý khách vui lòng liên hệ tổng đài 19006457 hoặc vào phần Đăng ký tư vấn trên SmartOne để gặp nhân viên tư vấn. 

##faq_54
* faq/faq_54
  -  Để tham khảo chính sách bảo hiểm và mua bảo hiểm online, Quý khách vui lòng làm theo hướng dẫn:\n- Bước 1: Đăng nhập vào SmartOne\n- Bước 2: Tại Tab sản phẩm tài chính --> chọn Bảo hiểm\n- Bước 3: Chọn sản phẩm bảo hiểm đang quan tâm và tham khảo chính sách chi tiết. \n- Bước 4: Điền thông tin các nhân, chụp ảnh chứng minh thư 2 mặt để xác nhận mua bảo hiểm. \n\nNgoài ra, Quý khách vui lòng liên hệ tổng đài 19006457 hoặc vào phần Đăng ký tư vấn trên SmartOne để gặp nhân viên tư vấn. 

##faq_55
* faq/faq_55
  -  Quý khách có thể yêu cầu Công ty bảo hiểm trả tiền bồi thường theo một trong hai hình thức sau: \n- Khách hàng thanh toán trước, Công ty Bảo hiểm chi trả sau\n- Bảo lãnh viện phí\n\nĐể tham khảo thông tin chi tiết về Hướng dẫn yêu cầu bồi thường, Quý khách vui lòng làm theo hướng dẫn: \n- Bước 1: Đăng nhập vào SmartOne\n- Bước 2: Tại Tab sản phẩm tài chính --> chọn Bảo hiểm\n- Bước 3: Chọn sản phẩm bảo hiểm đang quan tâm/sử dụng --> Tra cứu --> Thông tin chi tiết --> Hướng dẫn yêu cầu bồi thường.\nNgoài ra, Quý khách vui lòng liên hệ tổng đài 19006457 hoặc vào phần Đăng ký tư vấn trên SmartOne để gặp nhân viên tư vấn. 

