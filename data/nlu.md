## intent:greet
- Chào bạn
- Chào
- Mình chào bạn nhé
- Chào nha
- chào nhé
- T chào m nhé
- Xin chào
- Hello
- Hi
- Hello bạn
- Hi bạn
- chào ngày mới nha
- chào bạn, chào lại mình đi
- chào t chưa?
- t chào m ạ
- Nói chào t chưa?
- Chào chưa?
- Chưa chào à?
- Nói chào đi!
- Hế nhô
- Say hello
- Say hi
- Hello hello
- Chào ngươi
- Hello m
- bạn ơi
- Xin chaof

## intent:goodbye
- bye
- goodbye
- see you around
- see you later
- tạm biệt
- tạm biệt nhé
- chào tạm biệt bạn

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct
- Okay nhé
- Ok
- Okay nha
- Okay không?
- Được
- Oki
- Oki oki
- Ok ok
- Okay Okay
- Okay được thôi
- Okey nhé
- Oke
- kay kay

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really
- không phải
- không
- không chắc
- không hẳn
- no no
- oh no

## intent:nlu_yeucau_mo_tkck
- Tôi muốn mở tài khoản CK tại VPS
- Tôi muốn mở TKCK tại VPS
- tôi muốn mở tkck ở VPS

## intent:nlu_xacnhan_cmt
- CMT là 123456
- Số CMT của tôi là 12346578
- CMT của tôi là 123456789

## intent:nlu_xacnhan_ho_ten
- Tên tôi là Nguyễn Văn A
- Nguyễn Thị B
- Nguyen Van A

## intent:nlu_xacnhan_sdt
- 012380912
- SDT là 1209381084
- 012345678
