## happy path
* greet
  - utter_greet
  - utter_did_that_help

## sad path 1
* greet
  - utter_greet
  - utter_did_that_help
* affirm
  - utter_thankyou

## sad path 2
* greet
  - utter_greet
  - utter_did_that_help
* deny
  - utter_sorry

## say goodbye
* goodbye
  - utter_goodbye

## New Story
* faq
  - respond_faq
  - action_set_faq_slot

## New Story

* check_robot
    - utter_iamabot

## interactive_story_1
* greet
    - utter_greet
    - utter_did_that_help
* nlu_yeucau_mo_tkck
    - utter_cungcap_cmt
* nlu_xacnhan_cmt
    - utter_cungcap_ho_ten
* nlu_xacnhan_ho_ten
    - utter_cungcap_sđt
* nlu_xacnhan_sdt
    - utter_xacnhan_mo_tkck
